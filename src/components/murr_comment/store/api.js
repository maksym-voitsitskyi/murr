import axios from "axios";

/**
 * @param {Number} murrId Murren card id
 * @param {Number} page Number page
 */
export const fetchComments = async (murrId, page) => {
  try {
    const { data, status } = await axios.get(
      `/api/murr_comments/?murr=${murrId}&page=${page}`
    );

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Object} formData Object data form
 */
export const addComment = async (formData) => {
  try {
    const { data, status } = await axios.post(`/api/murr_comments/`, formData);

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Number} commentId Comment id
 */
export const likeComment = async (commentId) => {
  try {
    const { data, status } = await axios.post(
      `/api/murr_comments/${commentId}/like/`
    );

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};

/**
 * @param {Number} commentId Comment id
 */
export const unlikeComment = async (commentId) => {
  try {
    const { data, status } = await axios.post(
      `/api/murr_comments/${commentId}/dislike/`
    );

    return { success: true, status, data };
  } catch ({ message, response: { status } }) {
    return { success: false, status, message };
  }
};
