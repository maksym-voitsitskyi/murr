from django.apps import AppConfig


class MurrWebsocketConfig(AppConfig):
    name = 'murr_websocket'

    # def ready(self):
    #     import murr_websocket.signals  # noqa
