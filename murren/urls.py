from django.conf.urls import url
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from murren.views import FacebookLogin, GoogleLogin, MurrenViewSet, VkLogin

router = DefaultRouter()
router.register('', MurrenViewSet, basename='murrens')

urlpatterns = [
    url('token_create/', obtain_jwt_token, name='obtain_jwt_token'),
    path('oauth/google/', GoogleLogin.as_view(), name='socialaccount_signup_google'),
    path('oauth/vk/', VkLogin.as_view(), name='socialaccount_signup_vk'),
    path('oauth/facebook/', FacebookLogin.as_view(), name='socialaccount_signup_facebook'),
]

urlpatterns += router.urls
