import pytest
from django.urls import reverse
from rest_framework import status


pytestmark = [
    pytest.mark.django_db,
]


def test_murren_create_comment_for_murr(create_release_murr, murr_comment_data, api_client):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']
    api_client.force_authenticate(user=murren)

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
    }

    url = reverse('comment-list')
    response = api_client.post(url, data, format='json')
    assert response.status_code == status.HTTP_201_CREATED

    url = reverse('comment-detail', kwargs={'pk': response.json()['id']})
    response = api_client.get(url)
    murr_comment = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert murr_comment['author_username'] == murren.username
    assert murr_comment['murr'] == murr.pk
    assert murr_comment['text'] == data['text']


def test_create_comment_for_murr_not_authenticated(
        create_release_murr, murr_comment_data, api_client
):
    murren = create_release_murr['murren']
    murr = create_release_murr['murr_card']

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
    }

    url = reverse('comment-list')
    response = api_client.post(url, data, format='json')
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_create_comment_for_comment(create_murr_comment, murr_comment_data, api_client):
    murren = create_murr_comment['murren']
    murr = create_murr_comment['murr_card']
    murr_comment = create_murr_comment['murr_comment']
    api_client.force_authenticate(user=murren)

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
        "parent": murr_comment.pk,
    }

    url = reverse('comment-list')
    response = api_client.post(url, data, format='json')
    children_comment = response.json()
    assert response.status_code == status.HTTP_201_CREATED

    url = reverse('comment-detail', kwargs={'pk': murr_comment.pk})
    response = api_client.get(url)
    murr_comment = response.json()
    assert response.status_code == status.HTTP_200_OK
    assert murr_comment['children'][0] == children_comment


def test_create_comment_for_comment_not_authenticated(
        create_murr_comment, murr_comment_data, api_client
):
    murren = create_murr_comment['murren']
    murr = create_murr_comment['murr_card']
    murr_comment = create_murr_comment['murr_comment']

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
        "parent": murr_comment.pk,
    }

    url = reverse('comment-list')
    response = api_client.post(url, data, format='json')
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


def test_create_comment_when_murren_is_banned(
        create_murren_is_banned, create_release_murr, murr_comment_data, api_client
):
    murren = create_murren_is_banned
    murr = create_release_murr['murr_card']
    api_client.force_authenticate(user=murren)

    data = {
        **murr_comment_data,
        "author": murren.pk,
        "murr": murr.pk,
    }

    url = reverse('comment-list')
    response = api_client.post(url, data, format='json')
    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_delete_comment_for_murr(api_client, create_murr_comment):
    murren = create_murr_comment['murren']
    murr_comment = create_murr_comment['murr_comment']
    api_client.force_authenticate(user=murren)

    url = reverse('comment-detail', kwargs={'pk': murr_comment.id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT

    response = api_client.get(url).json()
    assert response['deleted'] is not None


def test_delete_comment_for_murr_not_authenticated(api_client, create_murr_comment):
    murr_comment = create_murr_comment['murr_comment']

    url = reverse('comment-detail', kwargs={'pk': murr_comment.id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    response = api_client.get(url).json()
    assert response['deleted'] is None


@pytest.mark.parametrize('count', [1])
def test_delete_comment_for_murr_by_another_user(api_client, create_murren_list, create_murr_comment):
    murren = create_murren_list[0]
    murr_comment = create_murr_comment['murr_comment']
    api_client.force_authenticate(user=murren)

    url = reverse('comment-detail', kwargs={'pk': murr_comment.id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN

    response = api_client.get(url).json()
    assert response['deleted'] is None


def test_delete_comment_for_comment(api_client, create_child_murr_comment):
    murren = create_child_murr_comment['murren']
    child_murr_comment = create_child_murr_comment['child_murr_comment']
    api_client.force_authenticate(user=murren)

    url = reverse('comment-detail', kwargs={'pk': child_murr_comment.id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_204_NO_CONTENT

    response = api_client.get(url).json()
    assert response['deleted'] is not None


def test_delete_comment_for_comment_not_authenticated(api_client, create_child_murr_comment):
    child_murr_comment = create_child_murr_comment['child_murr_comment']

    url = reverse('comment-detail', kwargs={'pk': child_murr_comment.id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    response = api_client.get(url).json()
    assert response['deleted'] is None


@pytest.mark.parametrize('count', [1])
def test_delete_comment_for_comment_by_another_user(api_client, create_murren_list, create_child_murr_comment):
    murren = create_murren_list[0]
    child_murr_comment = create_child_murr_comment['child_murr_comment']
    api_client.force_authenticate(user=murren)

    url = reverse('comment-detail', kwargs={'pk': child_murr_comment.id})
    response = api_client.delete(url)
    assert response.status_code == status.HTTP_403_FORBIDDEN

    response = api_client.get(url).json()
    assert response['deleted'] is None
