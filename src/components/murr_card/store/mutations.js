import * as type from "./type.js";
import { defaultMurrCardData } from "./index.js";

export default {
  [type.MURR_CARDS_APPEND]: (state, payload) => {
    state.murrList = [...state.murrList, ...payload.results];
    state.murrListCount = payload.count;
  },

  [type.MURR_CARDS_ADD]: (state, payload) => {
    state.murrList = [payload, ...state.murrList];
    state.murrListCount = state.murrListCount + 1;
  },

  [type.MURR_CARDS_CLEAR]: (state) => (state.murrList = []),
  [type.CLEAR_SUBSCRIBE_CARDS]: (state, murrenId) =>
    (state.murrList = state.murrList.filter((e) => e.owner !== murrenId)),
  [type.MURR_CARDS_REMOVE]: (state, murrID) => {
    state.murrList = state.murrList.filter((item) => item.id !== murrID);
    state.murrListCount = state.murrListCount - 1;
  },
  [type.MURR_CARDS_CHANGE_SUBSCRIBER_STATUS]: (
    state,
    { murrenID, isSubscriber }
  ) => {
    state.murrList = state.murrList.map((item) => {
      if (item.owner === murrenID) {
        item.is_subscriber = isSubscriber;
      }
      return item;
    });
  },

  [type.MURR_CARD_SET]: (state, payload) => (state.murr = payload),

  [type.MURR_CARD_CLEAR]: (state) => (state.murr = null),

  [type.MURR_CARD_SET_LOCAL_STORAGE]: (state, payload) => {
    const object = JSON.parse(state.murrDataLocalStorage);

    state.murrDataLocalStorage = JSON.stringify({
      ...object,
      ...payload,
    });
  },

  [type.MURR_CARD_CLEAR_LOCAL_STORAGE]: (state) =>
    (state.murrDataLocalStorage = JSON.stringify(defaultMurrCardData)),

  [type.MURR_CARD_SET_CATEGORIES]: (state, payload) =>
    (state.murrCategories = payload),
};
