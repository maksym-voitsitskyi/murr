import mutations from "./mutations.js";

export const optionsDefault = {
  positionBtnClose: "left",
};

export default {
  namespaced: true,
  state: {
    isOpen: false,
    payload: null,
    transition: null,
    options: optionsDefault,
  },
  getters: {
    isOpen: (state) => state.isOpen,
    payload: (state) => state.payload,
    transition: (state) => state.transition,
    options: (state) => state.options,
  },
  mutations,
};
