FROM python:3.7-slim
WORKDIR /home/murrengan

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install netcat -y
RUN apt-get install libwebp-dev -y
RUN pip install --upgrade pip

COPY requirements/base.txt /home/murrengan/requirements.txt
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY . /home/murrengan

ENTRYPOINT ["/home/murrengan/entrypoint.sh"]
