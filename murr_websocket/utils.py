import re


def clean_html(html, allow_tags=['p', 'b', 'i', 'u', 'br'], allow_attrs=False):
    allow_tags = (f'\/?{i}' for i in allow_tags)
    if allow_attrs:
        # remove all tags except those allowed WITH attributes
        allow_tags = '|'.join(allow_tags)
    else:
        # remove all tags except those allowed WITHOUT attributes
        allow_tags = '>|'.join(allow_tags) + '>'

    tag = r'<(?!%s).*?>' % allow_tags
    html = re.sub(tag, '', html)
    return html


def wrap_links_in_tags(text):
    protocols = ['http://', 'https://']
    pattern = r'((https?:\/\/)?(([\w-]+\.)+[\w-]+)(.*?)(?=\s|<\/?.+>|$))'
    find_links = re.findall(pattern, text)
    for link in set(find_links):
        protocol = 'https://' if link[1] not in protocols else ''
        link_in_tag = f'<a href="{protocol}{link[0]}" target="_blank">{link[0]}</a>'
        text = text.replace(link[0], link_in_tag)

    return text
