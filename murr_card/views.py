import json
from django.core.files.images import ImageFile
from django.db.models import Count
from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import status

from murr_back.settings import BACKEND_URL
from murr_card.models import Category, MurrCard, MurrCardStatus
from murr_card.permissions import IsAuthenticatedAndOwnerOrReadOnly
from murr_card.serializers import (
    EditorImageForMurrCardSerializers,
    MurrCardCategoriesSerializer,
    MurrCardSerializers,
)
from murr_rating.services import RatingActionsMixin
from murren.services import make_resize_image, make_crop_image

Murren = get_user_model()


class MurrPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = "murr_card_len"
    max_page_size = 60


class MurrCardViewSet(RatingActionsMixin, ModelViewSet):
    serializer_class = MurrCardSerializers
    permission_classes = [IsAuthenticatedAndOwnerOrReadOnly]
    pagination_class = MurrPagination
    filter_backends = [DjangoFilterBackend]
    filter_fields = ["owner", "category__slug"]

    def get_queryset(self):
        queryset = (
            MurrCard.objects.select_related("owner")
            .annotate(
                likes=Count("liked_murrens", distinct=True),
                dislikes=Count("disliked_murrens", distinct=True),
                comments_count=Count("comments", distinct=True),
            )
            .filter(status=MurrCardStatus.RELEASE)
            .order_by("-timestamp")
        )

        return queryset

    def create(self, request, *args, **kwargs):

        data = request.data.copy()

        category = Category.objects.filter(slug=data.pop("category", None)).first()
        if category is not None:
            data["category"] = category.slug

        data["owner"] = request.user.id
        data["status"] = request.data.get("status", MurrCardStatus.RELEASE.value)

        file_cover = data.get("cover")
        if file_cover:
            cover_crop_detail = json.loads(data.get("coverCropDetail"))

            crop_x = int(cover_crop_detail["x"])
            crop_y = int(cover_crop_detail["y"])
            crop_width = int(cover_crop_detail["width"])
            crop_height = int(cover_crop_detail["height"])

            b_image, error = make_crop_image(file_cover, (crop_x, crop_y, crop_width, crop_height))

            if error:
                return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

            data["cover"] = ImageFile(file=b_image, name="tmp_cover_name.jpeg")

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=False)
    def category_list(self, request):
        queryset = Category.objects.all()
        queryset = self.paginate_queryset(queryset)
        serializer = MurrCardCategoriesSerializer(queryset, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=False, permission_classes=[IsAuthenticated])
    def my_likes(self, request):
        queryset = self.get_queryset().filter(liked_murrens=request.user)
        return self.paginate_murr_card(queryset)

    @action(detail=False, permission_classes=[IsAuthenticated])
    def feed(self, request):
        subscriptions = request.user.subscriptions.all()
        queryset = self.get_queryset().filter(owner__in=subscriptions)
        return self.paginate_murr_card(queryset)

    def paginate_murr_card(self, queryset):
        cards_on_page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(cards_on_page, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=False, permission_classes=[IsAuthenticated], methods=["POST"])
    def cover_resize(self, request):
        file_cover = request.FILES.get("cover")
        b_image, error = make_resize_image(file_cover, (600, 600))
        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return HttpResponse(b_image, content_type="image/jpeg")


class EditorImageForMurrCardView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = EditorImageForMurrCardSerializers(data=request.data)
        murr_dict = {"success": 0, "file": {"url": ""}}
        if serializer.is_valid():
            serializer.save()
            url = BACKEND_URL + serializer.data["murr_editor_image"]
            murr_dict = {"success": 1, "file": {"url": url}}
        return Response(murr_dict)
