from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from rest_framework import serializers

from murr_card.serializers import MurrCardSerializers
from murr_notifications.models import FeedNotification, SubscribeNotification
from murren.serializers import MurrenSerializer


class SubscribeNotificationSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    to_murren = serializers.CharField(source='to_murren.username')
    from_murren = serializers.SerializerMethodField()

    class Meta:
        model = SubscribeNotification
        fields = ('id', 'sent_time', 'to_murren', 'from_murren', 'notification_type',)

    def get_from_murren(self, obj) -> dict:
        return MurrenSerializer(obj.subscribenotification.from_murren, context=self.context).data


class FeedNotificationSerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    to_murren = serializers.CharField(source='to_murren.username')
    murr_card = serializers.SerializerMethodField()

    class Meta:
        model = FeedNotification
        fields = ('id', 'sent_time', 'to_murren', 'murr_card', 'notification_type',)

    def get_murr_card(self, obj) -> dict:
        return MurrCardSerializers(obj.feednotification.murr_card, context=self.context).data
