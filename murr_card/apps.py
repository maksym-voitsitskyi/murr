from django.apps import AppConfig


class MurrCardConfig(AppConfig):
    name = 'murr_card'

    def ready(self):
        import murr_card.signals
