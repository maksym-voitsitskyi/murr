from rest_framework import permissions


class IsAuthenticatedAndMurrenOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return bool(request.user and request.user.is_authenticated and not request.user.is_banned)

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return bool(obj == request.user and not request.user.is_banned)


class IsNotProfileOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        return bool(obj != request.user)
